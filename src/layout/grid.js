import * as React from "react";
import styled from "styled-components";

export const Grid = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const grid = (props) => <Grid>{props.children}</Grid>;

export default grid;
