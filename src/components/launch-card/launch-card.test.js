import React from "react";
import {render} from "@testing-library/react";

import {LaunchCard} from ".";

const props = {
  key: 1,
  image: "https://via.placeholder.com/150",
  description: "Card description",
  title: "Title",
};

const setup = ({...data}) => {
  const component = render(<LaunchCard {...data} />);

  return {
    ...component,
  };
};

describe("LaunchCard", () => {
  describe("Description", () => {
    it("Should have a description", () => {
      const {queryByText} = setup(props);
      expect(queryByText(props.description)).toBeInTheDocument();
    });
    it("Should not have a description", () => {
      const {queryByText} = setup({...props, description: ""});
      expect(queryByText(props.description)).not.toBeInTheDocument();
    });
  });

  describe("Title", () => {
    it("Should have a title", () => {
      const {queryByText} = setup(props);
      expect(queryByText(props.title)).toBeInTheDocument();
    });
    it("Should not have a title", () => {
      const {queryByText} = setup({...props, title: ""});
      expect(queryByText(props.title)).not.toBeInTheDocument();
    });
  });

  describe("Image", () => {
    it("Should have an image", () => {
      render(<LaunchCard {...props} />);
      const image = document.querySelector("img");
      expect(image.src).toContain("via.placeholder.com/150");
    });
  });
});
