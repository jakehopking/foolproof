import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Image from "../image";
import Lockup from "../lockup";

const LaunchCardWrapper = styled.div`
  display: flex;
  margin-bottom: 30px;
  margin-right: 30px;
  flex: 0 1 calc(33.33% - 30px);
  align-items: flex-start;
`;

const LaunchCardContainer = styled.div`
  display: flex;
  flex: 1;
  flex-flow: column-reverse;
  height: 100%;
`;

const ImagContainer = styled.div`
  padding: 40px 20px;
  background-color: #b3c7cc;
  position: relative;
  margin-top: auto;
  width: 100%;

  img {
    height: 100px;
    width: auto;
    display: block;
    margin: 0 auto;
  }
`;

const Content = styled.div`
  padding: 20px;
  background-color: #f6f7f7;
  flex: 1;
  width: 100%;
`;

function LaunchCard({image, description, title}) {
  return (
    <LaunchCardWrapper>
      <LaunchCardContainer>
        <ImagContainer>
          <Image url={image} />
        </ImagContainer>

        <Content>
          <Lockup text={description} tag="h3" title={title} />
        </Content>
        {/* Youtube Link ? */}
      </LaunchCardContainer>
    </LaunchCardWrapper>
  );
}

export default LaunchCard;

LaunchCard.propsTypes = {
  image: PropTypes.string,
  description: PropTypes.string,
  title: PropTypes.string,
};
