// Server

const express = require("express");
const app = express();
const port = 4000;

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/launches", (req, res) => {
  res.send("GET HTTP method on launches resource");
});

app.get("/rockets", (req, res) => {
  res.send("GET HTTP method on rockets resource");
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
